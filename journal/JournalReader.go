package journal

import (
	"bytes"
	"encoding/json"
	"github.com/pkg/errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"time"
)

type Event struct {
	Timestamp time.Time `json:"timestamp"`
	Event     string    `json:"event"`

	Raw          []interface{} `json:"Raw"`
	Manufactured []interface{} `json:"Manufactured"`
	Encoded      []interface{} `json:"Encoded"`
}

func FindMostRecentMaterialEvent(journalPath string) (*Event, error) {
	files, err := filepath.Glob(journalPath + "/Journal.*.*.log")
	if err != nil {
		return nil, errors.Wrap(err, "error while listing files in journal path")
	}

	filesByTime := make(map[int]string)
	timesInOrder := make([]int, 0)

	for _, file := range files {
		info, err := os.Stat(file)
		if err != nil {
			return nil, errors.Wrap(err, "error while reading file time")
		}
		fTime := int(info.ModTime().Unix())
		timesInOrder = append(timesInOrder, fTime)
		filesByTime[fTime] = file
	}
	sort.Sort(sort.Reverse(sort.IntSlice(timesInOrder)))

	for _, fTime := range timesInOrder {
		fileData, err := ioutil.ReadFile(filesByTime[fTime])
		if err != nil {
			return nil, errors.Wrap(err, "error while reading journal file")
		}
		lines := bytes.Split(fileData, []byte{'\n'})

		for i := len(lines) - 2; i >= 0; i-- {
			var data Event
			err = json.Unmarshal(lines[i], &data)
			if err != nil {
				return nil, errors.Wrap(err, "error while reading journal json")
			}
			if data.Event == "Materials" {
				return &data, nil
			}
		}
	}

	return nil, errors.New("No material event found")
}
