package main

import (
	"./journal"
	"encoding/json"
	"flag"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"math"
	"sort"
	"strings"
	"time"
)

var (
	requirements map[string]int
	materials    map[string]int

	Results []Entry
)

type Entry struct {
	Name       string
	Required   int
	Have       int
	Percentage float64
}

func main() {
	journalPath := flag.String("journal", "", "The path where the journal logs can be found.")
	requirementsFile := flag.String("requirements", "requirements.json", "The file containing your shopping list.")
	flag.Parse()

	err := reloadRequirements(*requirementsFile)
	if err != nil {
		panic(err)
	}

	ticker := time.NewTicker(time.Second * 2)
	go func() {
		for range ticker.C {
			var err error
			latestEvent, err := journal.FindMostRecentMaterialEvent(*journalPath)
			if err != nil {
				panic(err)
			}

			materials = make(map[string]int)
			for _, mat := range latestEvent.Raw {
				m := mat.(map[string]interface{})
				materials[strings.Title(m["Name"].(string))] = int(m["Count"].(float64))
			}
			for _, mat := range latestEvent.Manufactured {
				m := mat.(map[string]interface{})
				materials[m["Name_Localised"].(string)] = int(m["Count"].(float64))
			}
			for _, mat := range latestEvent.Encoded {
				m := mat.(map[string]interface{})
				materials[m["Name_Localised"].(string)] = int(m["Count"].(float64))
			}

			updateResults()
		}
	}()

	r := gin.Default()
	r.LoadHTMLFiles("overlay.html")
	r.GET("/ping", func(c *gin.Context) {
		c.HTML(200, "overlay.html", Results)
	})
	r.GET("/reloadRequirements", func(c *gin.Context) {
		err := reloadRequirements(*requirementsFile)
		if err != nil {
			c.JSON(500, gin.H{"error": err.Error()})
		}
		c.Status(200)
		updateResults()
	})

	err = r.Run()
	if err != nil {
		panic(err)
	}
}

func reloadRequirements(requirementsFile string) error {
	fileData, err := ioutil.ReadFile(requirementsFile)
	if err != nil {
		return err
	}

	err = json.Unmarshal(fileData, &requirements)
	if err != nil {
		return err
	}

	return nil
}

func updateResults() {
	Results = make([]Entry, 0, len(requirements))
	for mat, count := range requirements {
		have, _ := materials[mat]

		e := Entry{
			Name:       mat,
			Have:       have,
			Required:   count,
			Percentage: float64(have) / float64(count) * 100,
		}
		Results = append(Results, e)
	}

	sort.Slice(Results, func(i, j int) bool {
		pA := Results[i].Percentage
		if pA >= 100 {
			pA = -900
		}
		pB := Results[j].Percentage
		if pB >= 100 {
			pB = -900
		}
		if math.Abs(pA-pB) < .1 ||
			pA > pB {
			return true
		}
		return false
	})
}
